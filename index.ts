const selection: string[] = ['rock', 'paper', 'scissors'];

function returnRandomSelection(): string {
  return selection[Math.floor(Math.random() * selection.length)];
}

function playRound(playerSelection: string, computerSelection: string) {
  return compare(playerSelection, computerSelection);
}

function compare(a: string, b: string) {
  if (a === b) {
    return 'The result is a tie!';
  }

  if (a === 'rock') {
    return b === 'scissors' ? 'rock wins' : 'paper wins';
  }

  if (a === 'paper') {
    return b === 'rock' ? 'paper wins' : 'scissors wins';
  }

  if (a === 'paper') {
    return b === 'scissors' ? 'scissors wins' : 'rock wins';
  }
}

function computerPlay(): string {
  return returnRandomSelection();
}

function playerPlay(): string {
  return returnRandomSelection();
}

const playerSelection = playerPlay();
const computerSelection = computerPlay();

function game() {
  for (let i = 0; i < 5; i++) {
    console.log(playRound(playerSelection, computerSelection));
  }
}

game();
